const bodyParser = require('body-parser')
var express = require('express')
var app = express()

port = process.env.PORT || 5000
mongoose = require('mongoose')

Contact = require('./api/model/Model')
//bodyParser = require('body-parser')   /* libery สำหรับ post */

mongoose.Promise = global.Promise
mongoose.connect("mongodb+srv://note2811:note2811@cluster0.m0dwa.mongodb.net/Squidd?retryWrites=true&w=majority", function(err){
    if(err) throw err
    console.log("Success Connect")
})

/* จะเปิด 2 เซิฟพร้อมกัน ต้องรัน lib ตัวนี้ */
const cors = require('cors')
app.use(cors())


app.use(bodyParser.urlencoded({extended: true})) /* ใช้ bodyParser รับ JSON*/
app.use(bodyParser.json())

var routes = require('./api/routes/Route')
routes(app)
app.listen(port) /* ใช้ port ที่ประกาศไว้ตอนแรก */
console.log('User List Server Stort on :'+port)