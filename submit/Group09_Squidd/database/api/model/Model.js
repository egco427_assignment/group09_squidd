/* สร้างเพื่อรับข้อมูลจาก database */

var mongoose  = require('mongoose')

/* สร่าง Schema เพื่อรับข้อมูล (เหมือน class ใน program)*/
var Schema = mongoose.Schema



var CountSchema = new Schema({
    count: { type: Number, default: 0 },
    id: [{ type: String, default: "" }]

})

var UserforPostSchema = new Schema({
    uid: { type: String , Required: 'Please enter id'},
    name: { type: String, Required: 'Please enter name'},
    coverImg: { type: String, default: "https://semantic-ui.com/images/wireframe/image.png"},

})

var CommentSchema = new Schema({
    userdata: { type: UserforPostSchema, Required: 'User' },
    text: { type: String, defualt: "" }
})

var PostSchema  = new Schema({
    Text:     { type: String, Required: 'Please enter Text'},
    ImageURL: { type: String, default: "https://semantic-ui.com/images/wireframe/image.png" },
    userdata:      { type: UserforPostSchema, Required: 'User' },
    isSubc:   { type: Boolean, default: false},
    createdDate: { type: Date, default: Date.now() },
    favlist: { type: CountSchema, default: () => ({}) },
    repostlist: { type: CountSchema, default: () => ({}) },
    comment: [{ type: CommentSchema, default: () => ({}) }]
})

var UserSchema = new Schema({
    name: { type: String, Required: 'Please enter name'},
    email: { type: String, Required: 'Please enter email'},
    password: { type: String, Required: 'Please enter password'},
    coverImg: { type: String, default: "https://semantic-ui.com/images/wireframe/image.png"},
    Subscript: { type: CountSchema, default: () => ({}) },
    postid: [{ type: String, default: "" }],
    favpost: { type: CountSchema, default: () => ({}) },
    
})



module.exports = mongoose.model('Posts', PostSchema, 'Post')
module.exports = mongoose.model('Users', UserSchema, 'User')
module.exports = mongoose.model('CommentSchema', CommentSchema)
// module.exports = mongoose.model('Accounts', AccountSchema, 'Accounts')