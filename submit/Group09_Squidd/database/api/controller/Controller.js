
const { query } = require('express')
var mongoose = require('mongoose')
Posts = mongoose.model('Posts')
Users = mongoose.model('Users')
CommentSchema = mongoose.model('CommentSchema')

exports.listAllPost = function(req, res){
    var query = {sort: {createdDate : -1}}
    Posts.find({}, null, query, function(err, post){
        if(err) throw err
        console.log(post)
        res.json(post)
    })
}

exports.listAllPostfromPostlist = function(req, res){
    var query = {sort: {createdDate : -1 }}
    var ids = req.body 
    Posts.find().where('_id').in(ids).exec((err, records) => {
        if(err) throw err
        console.log(post)
        res.json(post)
    })
}

exports.CreatePost = function(req, res){
    var obj = new Posts(req.body) // ดึงค่าจาก body ของ postman
    obj.save( function(err, post){
        if(err) throw err
        console.log(Date.now())
        console.log(post)
        res.json(post)
    })
}

exports.readPost= function(req, res){
    let query = req.params.pid
    
    Posts.findById(query , function(err, post){
        if(err) throw err
        //console.log(user)
        res.json(post)
    })  
}

exports.delPost = function(req, res){

    Posts.findOneAndRemove( req.params.pid ,function(err, contact){
        if(err) throw err
        const response = {
            message: "Delete successfully",
            //id: user._id
        }
        res.json(response)
    })
}

exports.AddCommenttoPost = function(req, res){
    var obj = new CommentSchema(req.body)

    var update = { $push: { "comment": obj } }

    Posts.findByIdAndUpdate(req.params.pid, update, function(err, post){
        if(err) throw err
        console.log(post)
        res.json(post)
    })
}

exports.delCommentfromPost = function(req, res){

    var query = { "_id": req.params.pid  }
    var update = { $pull: { "comment" : { "_id": req.params.cid }}}

    Posts.update(query, update, function(err, post){
        if(err) throw err
        //console.log(post)
        res.json(post)
        
    })
}

exports.Favpost = function(req, res){    

    var uuid = req.params.uid

    Posts.findById(req.params.pid, function(err, post){
        if(err) throw err
        //console.log(post)
        let list = post.favlist.id
        list.addToSet(uuid)
        post.favlist.count = list.length

        post.save( function(err, acc){
            if(err) throw err
            //console.log(acc)
            res.json(acc)
        })
    })
}

exports.Favdelpost = function(req, res){    

    var uuid = req.params.uid

    Posts.findById(req.params.pid, function(err, post){
        if(err) throw err
        //console.log(post)
        let list = post.favlist.id
        list.pull(uuid)
        post.favlist.count = list.length

        post.save( function(err, acc){
            if(err) throw err
            //console.log(acc)
            res.json(acc)
        })
    })
}

exports.Repost = function(req, res){    

    var uuid = req.params.uid

    Posts.findById(req.params.pid, function(err, post){
        if(err) throw err
        //console.log(post)
        let list = post.repostlist.id
        list.addToSet(uuid)
        post.repostlist.count = list.length

        post.save( function(err, acc){
            if(err) throw err
            //console.log(acc)
            res.json(acc)
        })
    })
}

exports.Redelpost = function(req, res){    

    var uuid = req.params.uid

    Posts.findById(req.params.pid, function(err, post){
        if(err) throw err
        //console.log(post)
        let list = post.repostlist.id
        list.pull(uuid)
        post.repostlist.count = list.length

        post.save( function(err, acc){
            if(err) throw err
            //console.log(acc)
            res.json(acc)
        })
    })
}
///////////////////////////////////////////////////////////////////////////////////////


exports.listAllUser = function(req, res){
    Users.find({}, null, function(err, post){
        if(err) throw err
        console.log(post)
        res.json(post)
    })
}


exports.UserEdit = function(req, res){

    var edit = {}
    edit = req.body
    console.log(edit)
    Users.findByIdAndUpdate(req.params.uid, edit, {new: true}, function(err, acc){
        if(err) throw err
        console.log(acc)
        res.json(acc)
    })
}

exports.readUser = function(req, res){
    let query = req.params.uid
    
    Users.findById(query , function(err, acc){
        if(err) throw err
        //console.log(user)
        res.json(acc)
    })  
}

exports.CreateUser = function(req, res){
    var obj = new Users(req.body) // ดึงค่าจาก body ของ postman
    obj.save( function(err, acc){
        if(err) throw err
        console.log(acc)
        res.json(acc)
    })
}

exports.UserUpdatepostlist = function(req, res){    
        var updatepostlist = { $addToSet: {postid: req.params.pid} }    
        Users.findByIdAndUpdate(req.params.uid, updatepostlist, {new: true}, function(err, user){
            if(err) throw err
            console.log(user)
            res.json(user)
        })
    }

exports.UserUpdateDelpostlist = function(req, res){    
        var updatepostlist = { $pull: {postid: req.params.pid} }    
        Users.findByIdAndUpdate(req.params.uid, updatepostlist, {new: true}, function(err, user){
            if(err) throw err
            console.log(user)
            res.json(user)
        })
    }

exports.Subscript = function(req, res){    

    Users.findById(req.params.myuid, function(err, acc){
        if(err) throw err
        //console.log(post)
        let list = acc.Subscript.id
        list.addToSet(req.params.subuid)
        acc.Subscript.count = list.length

        acc.save( function(err, acc){
            if(err) throw err
            //console.log(acc)
            res.json(acc)
        })
    })
}

exports.unSubscript = function(req, res){    

    Users.findById(req.params.myuid, function(err, acc){
        if(err) throw err
        //console.log(post)
        let list = acc.Subscript.id
        list.pull(req.params.subuid)
        acc.Subscript.count = list.length

        acc.save( function(err, acc){
            if(err) throw err
            //console.log(acc)
            res.json(acc)
        })
    })
}

exports.Favuser = function(req, res){    

    Users.findById(req.params.myuid, function(err, acc){
        if(err) throw err
        //console.log(post)
        let list = acc.favpost.id
        list.addToSet(req.params.pid)
        acc.favpost.count = list.length

        acc.save( function(err, acc){
            if(err) throw err
            //console.log(acc)
            res.json(acc)
        })
    })
}

exports.unFavuser = function(req, res){    

    Users.findById(req.params.myuid, function(err, acc){
        if(err) throw err
        //console.log(post)
        let list = acc.favpost.id
        list.pull(req.params.pid)
        acc.favpost.count = list.length

        acc.save( function(err, acc){
            if(err) throw err
            //console.log(acc)
            res.json(acc)
        })
    })
}