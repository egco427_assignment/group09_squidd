'use strict'

module.exports = function(app){
    var controller  =require('../controller/Controller')

    /*  */
    app.route('/posts/list')
    .get(controller.listAllPost)

    app.route('/posts/listonly')
    .get(controller.listAllPostfromPostlist)

    app.route('/posts/create')
    .post(controller.CreatePost)

    app.route('/posts/:pid')
    .get(controller.readPost)
    .delete(controller.delPost)

    app.route('/posts/comment/:pid')
    .put(controller.AddCommenttoPost)

    app.route('/posts/comment/:pid/:cid')
    .put(controller.delCommentfromPost)

    app.route('/posts/fav/:pid/:uid')
    .put(controller.Favpost)

    app.route('/posts/unfav/:pid/:uid')
    .put(controller.Favdelpost)

    app.route('/posts/re/:pid/:uid')
    .put(controller.Repost)

    app.route('/posts/redel/:pid/:uid')
    .put(controller.Redelpost)

    // app.route('/posts/repost/:uid')
    // .put(controller.Reppost)

    app.route('/user/list')
    .get(controller.listAllUser)

    app.route('/user/:uid')
    .get(controller.readUser)

    app.route('/user/:uid')
    .put(controller.UserEdit)

    app.route('/user/addpost/:uid/:pid')
    .put(controller.UserUpdatepostlist)

    app.route('/user/delpost/:uid/:pid')
    .put(controller.UserUpdateDelpostlist)

    app.route('/user/create')
    .post(controller.CreateUser)

    app.route('/user/sub/:myuid/:subuid')
    .put(controller.Subscript)

    app.route('/user/unsub/:myuid/:subuid')
    .put(controller.unSubscript)

    app.route('/user/fav/:myuid/:pid')
    .put(controller.Favuser)

    app.route('/user/unfav/:myuid/:pid')
    .put(controller.unFavuser)
}