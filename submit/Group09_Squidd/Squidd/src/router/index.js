import { createRouter, createWebHistory } from 'vue-router'
import Home from '../components/main.vue'
import Signup from '../components/signup.vue'
import Subs from '../components/subscript.vue'
import Profile from '../components/profile.vue'
import SignIn from '../components/signIn.vue'
import VProfile from '../components/Vprofile.vue'
import onepost from '../components/onepost.vue'
import Discover from '../components/discover.vue'

const routerHistory = createWebHistory()

const routes = [
    {
        path :'/',
        redirect : '/home'
    },
    
    {
        path:'/home',
        name: 'Home',
        component: Home,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/signup',
        name: 'Signup',
        component: Signup
    },
    {
        path: '/subscription',
        name: Subs,
        component: Subs
    },
    // {
    //     path: '/profile',
    //     name: Profile,
    //     component: Profile
    // },
    {
        path: '/dicov',
        name: 'Discover',
        component: Discover
    },
    {
        path: '/login',
        name: 'SignIn',
        component: SignIn
    },
    {
        path: '/profile/:userId',
        //path: '/profiles',
        name: 'VProfile',
        component: VProfile
    },
    {
        path: '/content/:postId',
        name: 'onepost',
        component: onepost
    },


]

const router = createRouter({
    history: routerHistory,
    routes
})

// router.beforeEach((to, from, next) => {
//     const currentUser = firebase.auth().currentUser()
//     const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
//     if (requiresAuth && !currentUser) {
//       console.log("You are not authorized to access this area.");
//       next('login')
//     } else if (!requiresAuth && currentUser) {
//       console.log("You are authorized to access this area.");
//       next('home')
//     } else {
//       next()
//     }
//   })


export default router