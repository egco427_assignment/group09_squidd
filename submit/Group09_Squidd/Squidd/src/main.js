import { createApp } from 'vue'
import firebase from 'firebase'
import App from './App.vue'
import router from './router'
import './index.css'
//import 'bootstrap'


let app

  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyAVGlLI0DCfMdwHyhfAiNFvD_5chkjlmeg",
    authDomain: "egco427-9da00.firebaseapp.com",
    databaseURL: "https://egco427-9da00-default-rtdb.firebaseio.com",
    projectId: "egco427-9da00",
    storageBucket: "egco427-9da00.appspot.com",
    messagingSenderId: "675893966689",
    appId: "1:675893966689:web:88f3a4d73e115557b53720",
    measurementId: "G-BL4EKVC9K1"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged((user)=>{
    if(!app){
        app = createApp(App).use(router).mount('#app')
    }
})